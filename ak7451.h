#ifndef AK7451_H
#define AK7451_H

#include "mbed.h"

// List of OPCODEs
#define AK7451_OPCODE_WRITE_EEPROM          0x01    // Able to write a data on EEPROM
#define AK7451_OPCODE_READ_EEPROM           0x02    // Able to read an EEPROM
#define AK7451_OPCODE_WRITE_REGISTER        0x03    // Able to write a data on register
#define AK7451_OPCODE_READ_REGISTER         0x04    // Able to read a register
#define AK7451_OPCODE_CHANGE_MODE           0x05    // Able to change mode
#define AK7451_OPCODE_ANGLE_MEASURE_CODE    0x08    // Able to read angle measure in user mode
#define AK7451_OPCODE_READ_ANGLE            0x09    // Able to read an angle data

// Register Address Map
#define AK7451_REG_ANG                      0x00    // 12bit angle data
#define AK7451_REG_MAG                      0x01    // Magnetic flux density strength(1LSB/mT)
#define AK7451_REG_CHMD                     0x02    // For mode state
#define AK7451_REG_ERRMON                   0x03    // This register will show what kind of error is.
#define AK7451_REG_ID1                      0x04    // For ID data (EEPROM Only)
#define AK7451_REG_ID2                      0x05    // For ID data (EEPROM Only)
#define AK7451_REG_ZP                       0x06    // For set up angle zero point
#define AK7451_REG_RDABZ                    0x07    // For set up "Rotation direction",
#define AK7451_REG_MLK                      0x08    // For memory lock
#define AK7451_REG_ABNRM                    0x09    // For set up abnormal detection disable
#define AK7451_REG_UVW                      0x0A    // For set up "UVW output enable disable"

#define AK7451_READ_ANGLE_STATE_MD          0x80
#define AK7451_READ_ANGLE_STATE_P1          0x40
#define AK7451_READ_ANGLE_STATE_P2          0x20
#define AK7451_READ_ANGLE_STATE_ER          0x10

#define AK7451_BIT_MASK_ABZ_E               0x80    // ABZ output enable
#define AK7451_BIT_MASK_UVW_E               0x40    // UVW output enable

#define AK7451_ABNORMAL_STATE_NORMAL        0x03    // Abnormal state error code

#define AK7451_LEN_BUF_MAX					0x03	// 2 byte maximum, plus address byte

/**
 * This is a device driver of AK7451 with SPI interface.
 *
 * @note AK7451 is a high speed angle sensor IC manufactured by AKM.
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak7451.h"
 * 
 * 
 * int main() {
 *     // Creates an instance of SPI
 * }
 * @endcode
 */
class AK7451
{
public:

    /**
     * Available opration modes in AK7451.
     */
    typedef enum {
        AK7451_NORMAL_MODE  = 0x0000,   /**< Normal mode operation. */
        AK7451_USER_MODE    = 0x050F,   /**< User mode operation. */
    } OperationMode;

    /**
     * Status of function. 
     */
    typedef enum {
        SUCCESS,                    /**< The function processed successfully. */
        ERROR,                      /**< General Error */
        ERROR_IN_USER_MODE,         /**< Error in user mode. */
        ERROR_IN_NORMAL_MODE,       /**< Error in normal mode. */
        ERROR_PARITY,               /**< Parity bit error. */
        ERROR_ABNORMAL_STRENGTH,    /**< Abnormal strength error. */
    } Status;

    /**
     * Constructor.
     *
     */
    AK7451();

    /**
     * Destructor.
     *
     */
     ~AK7451();

    /**
     * begin
     *
     * @param *spi pointer to SPI instance
     * @param *cs pointer to DigitalOut instance for CS
     */
    void begin(SPI *spi, DigitalOut *cs);
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status checkConnection();

    /** 
     * Writes data to EEPROM on the device. 
     * @param address EEPROM address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status writeEEPROM(char address, const char *data);

    /** 
     *  Reads data from EEPROM on the device. 
     * @param address EEPROM address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readEEPROM(char address, char *data);

    /** 
     * Writes data to register on the device. 
     * @param address register address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status writeRegister(char address, const char *data);

    /** 
     *  Reads data from register on the device. 
     * @param address register address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readRegister(char address, char *data);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setOperationMode(OperationMode mode);

    /**
     * Gets device operation mode.
     *
     * @return Returns OperationMode.
     */
    OperationMode getOperationMode();

    /**
     * Reads angle data from the device.
     *
     * @param angle pointer to read angle data buffer
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readAngle(char *angle);
    
    /**
     * Measures and reads angle, magnetic flux density and abnormal state code while in the user mode.
     *
     * @param angle pointer to angle data buffer
     * @param density magnetic flux density
     * @param abnormal_state abnormal state
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readAngleMeasureCommand(char *angle, char *density, char *abnormal_state);
    
    /**
     * Measures current angle and sets the value to EEPROM as zero angle position.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setAngleZero();
    
    /**
     * Sets the value to EEPROM as zero angle position.
     *
     * @param angle zero angle position
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setAngleZero(const char *angle);

private:        
    /**
     * Holds a pointer to an SPI object. 
     */
    SPI *_spi;

    /**
     * Holds a DigitalOut oblject for CS; 
     */
    DigitalOut *_cs;
    
    /**
     * Holds current mode 
     */
    OperationMode operationMode;

    /** 
     *  Reads data from device. 
     * @param operation_code OPCODE 
     * @param address memory/register address
     * @param *data pointer to the read buffer, fixed length of 2.
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status read(char operation_code, char address, char *data);

    /** 
     * Writes data to the device. 
     * @param operation_code OPCODE 
     * @param address memory/register addredd
     * @param *data pointer to the read buffer. length=2 fixed.
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status write(char operation_code, char address, const char *data);

    /** 
     * Checks parity bits sub function
     * @param data data 12bit read data
     * @param parity parity bit status
     * @param error error bit status
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status parityCheckSub(const char data, const char parity, const char error);

    /** 
     * Checks parity bits
     * @param data 2 byte read data to chek parity
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status parityCheck(const char *data);
};

#endif
